package com.android.exomind.test.data.network

import com.android.exomind.test.data.model.CityWeather
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("weather")
    suspend fun getCityWeather(
        @Query("q") city: String,
        @Query("units") unit: String,
        @Query("appid") apiKey: String
    ): CityWeather
}