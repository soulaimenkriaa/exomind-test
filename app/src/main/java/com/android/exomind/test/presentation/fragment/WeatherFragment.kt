package com.android.exomind.test.presentation.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.android.exomind.test.R
import com.android.exomind.test.data.model.CityWeather
import com.android.exomind.test.databinding.FragmentWeatherBinding
import com.android.exomind.test.presentation.adapter.CityWeatherAdapter
import com.android.exomind.test.presentation.viewmodel.CityWeatherViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WeatherFragment : Fragment() {
    private var mBinding: FragmentWeatherBinding? = null
    private val mCityWeatherViewModel: CityWeatherViewModel by viewModels()
    private val mLoadingTextsArrayList = ArrayList<String>()
    private val mHandler = Handler(Looper.getMainLooper())
    private val mCityWeathersArrayList = ArrayList<CityWeather>()
    private lateinit var mCityWeatherAdapter: CityWeatherAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentView: View
        FragmentWeatherBinding.inflate(inflater, container, false).apply {
            fragmentView = root
            mBinding = this

            mBinding?.fragmentWeatherReloadButton?.setOnClickListener {
                startLoadingProgressBar()
            }

            init()
        }
        return fragmentView
    }

    private fun init() {
        mLoadingTextsArrayList.add(getString(R.string.loading_text_1))
        mLoadingTextsArrayList.add(getString(R.string.loading_text_2))
        mLoadingTextsArrayList.add(getString(R.string.loading_text_3))
        startLoadingProgressBar()
    }

    private fun startLoadingProgressBar() {
        mCityWeathersArrayList.clear()

        mBinding?.fragmentWeatherPercentageTextView?.visibility = View.VISIBLE
        mBinding?.fragmentWeatherReloadButton?.visibility = View.GONE
        mBinding?.fragmentWeatherLoadingTextView?.visibility = View.VISIBLE
        mBinding?.fragmentWeatherLoadingProgressBar?.visibility = View.VISIBLE
        mBinding?.fragmentWeatherCityWeatherRecyclerView?.visibility = View.GONE

        fetchWeatherData(0)
    }

    /**
     * Called every 10s to fetch weather data every time for a different city
     * When reaching 60s, it calls [populateWeatherData] to show results
     */
    private fun fetchWeatherData(second: Int) {
        if (second < 60) {
            // Update loading text every 6s
            if (second % 6 == 0) {
                mBinding?.fragmentWeatherLoadingTextView?.text =
                    when (mBinding?.fragmentWeatherLoadingTextView?.text) {
                        mLoadingTextsArrayList[0] -> mLoadingTextsArrayList[1]
                        mLoadingTextsArrayList[1] -> mLoadingTextsArrayList[2]
                        mLoadingTextsArrayList[2] -> mLoadingTextsArrayList[0]
                        else -> mLoadingTextsArrayList[0]
                    }
            }

            // Update progressBar progress value and percentage
            mBinding?.fragmentWeatherLoadingProgressBar?.progress = ((100 * second) / 60)
            mBinding?.fragmentWeatherPercentageTextView?.text =
                mBinding?.fragmentWeatherLoadingProgressBar?.progress.toString() + " %"

            // Fetch every 10s a city weather data
            if (second % 10 == 0 && second != 50) {
                getWeatherOfCities(second)
            }

            // Call this function every 1s until reaching 60s
            mHandler.postDelayed({ fetchWeatherData(second + 1) }, 1000)
        } else {
            mHandler.removeCallbacksAndMessages(null)
            mBinding?.fragmentWeatherLoadingProgressBar?.visibility = View.GONE
            mBinding?.fragmentWeatherLoadingTextView?.visibility = View.GONE
            mBinding?.fragmentWeatherPercentageTextView?.visibility = View.GONE
            mBinding?.fragmentWeatherReloadButton?.visibility = View.VISIBLE
            mBinding?.fragmentWeatherCityWeatherRecyclerView?.visibility = View.VISIBLE

            populateWeatherData()
        }
    }

    /**
     * Create [CityWeatherAdapter], set it to the corresponded RecyclerView and fill it with [CityWeather] data
     */
    private fun populateWeatherData() {
        if (!mCityWeathersArrayList.isNullOrEmpty()) {
            mCityWeatherAdapter = CityWeatherAdapter()
            mBinding?.fragmentWeatherCityWeatherRecyclerView?.apply {
                adapter =
                    mCityWeatherAdapter
            }
            mCityWeatherAdapter.setCityWeathers(mCityWeathersArrayList)
        }
    }

    private fun getWeatherOfCities(secondsUntilFinish: Int) {
        fetchCityWeather(
            when (secondsUntilFinish) {
                RENNES.first -> RENNES.second
                PARIS.first -> PARIS.second
                NANTES.first -> NANTES.second
                BORDEAUX.first -> BORDEAUX.second
                LYON.first -> LYON.second
                else -> ""
            }
        )
    }

    private fun fetchCityWeather(city: String) {
        lifecycleScope.launch(Dispatchers.Main) {
            mCityWeatherViewModel.getWeatherByCity(city)?.let {
                mCityWeathersArrayList.add(it)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mBinding = null
    }

    companion object {
        val RENNES = Pair(0, "Rennes")
        val PARIS = Pair(10, "Paris")
        val NANTES = Pair(20, "Nantes")
        val BORDEAUX = Pair(30, "Bordeaux")
        val LYON = Pair(40, "Lyon")
    }
}