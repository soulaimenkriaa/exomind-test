package com.android.exomind.test.data.network

import com.android.exomind.test.core.constant.GlobalConstants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private val mGson: Gson by lazy {
        GsonBuilder().setLenient().create()
    }

    private val mHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }).build()
    }

    private val mRetrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(GlobalConstants.API.OPEN_WEATHER_MAP_BASE_URL)
            .client(mHttpClient)
            .addConverterFactory(GsonConverterFactory.create(mGson))
            .build()
    }

    val mApiService: ApiService by lazy {
        mRetrofit.create(ApiService::class.java)
    }
}