package com.android.exomind.test.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.android.exomind.test.R
import com.android.exomind.test.databinding.FragmentHomeBinding

/**
 * Home fragment.
 */
class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentView: View
        FragmentHomeBinding.inflate(inflater, container, false).apply {
            fragmentView = root

            fragmentHomeStartButton.setOnClickListener {
                findNavController().navigate(
                    R.id.weatherFragment, null,
                    navOptions {
                        anim {
                            enter = android.R.animator.fade_in
                            exit = android.R.animator.fade_out
                        }
                    })
            }
        }
        return fragmentView
    }
}