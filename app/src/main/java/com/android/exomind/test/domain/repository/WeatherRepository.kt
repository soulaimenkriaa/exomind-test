package com.android.exomind.test.domain.repository

import com.android.exomind.test.data.model.CityWeather

interface WeatherRepository {
    /**
     * @return CityWeatherResponse
     */
    suspend fun fetchWeatherByCity(city: String): CityWeather?
}