package com.android.exomind.test.core.constant

class GlobalConstants {
    object API {
        const val OPEN_WEATHER_MAP_BASE_URL: String = "https://api.openweathermap.org/data/2.5/"
        const val OPEN_WEATHER_MAP_API_KEY: String = "4aa2dece2b83d0625c84ddd5aa8ddf26"
        const val OPEN_WEATHER_MAP_METRIC: String = "metric"
    }

    object CloudStatus {
        const val CLEAR = "Clear"
        const val CLOUDS = "Clouds"
    }
}