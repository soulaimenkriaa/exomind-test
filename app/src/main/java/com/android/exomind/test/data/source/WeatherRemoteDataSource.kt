package com.android.exomind.test.data.source

import com.android.exomind.test.core.constant.GlobalConstants.API.OPEN_WEATHER_MAP_API_KEY
import com.android.exomind.test.core.constant.GlobalConstants.API.OPEN_WEATHER_MAP_METRIC
import com.android.exomind.test.data.model.CityWeather
import com.android.exomind.test.data.network.ApiClient

/**
 * Weather network data source.
 */
object WeatherRemoteDataSource {

    /**
     * Retrieves weather by city
     */
    suspend fun fetchWeatherByCity(city: String): CityWeather? {
        return try {
            ApiClient.mApiService.getCityWeather(
                city,
                OPEN_WEATHER_MAP_METRIC,
                OPEN_WEATHER_MAP_API_KEY
            )
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}