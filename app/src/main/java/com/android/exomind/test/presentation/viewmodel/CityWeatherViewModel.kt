package com.android.exomind.test.presentation.viewmodel

import androidx.lifecycle.ViewModel
import com.android.exomind.test.data.model.CityWeather
import com.android.exomind.test.domain.usecase.FetchCityWeatherUsesCase

class CityWeatherViewModel : ViewModel() {
    private var fetchCityWeatherUsesCase = FetchCityWeatherUsesCase()

    /**
     *  Get weather by city
     */
    suspend fun getWeatherByCity(city: String): CityWeather? {
        return fetchCityWeatherUsesCase.invoke(city)
    }
}