package com.android.exomind.test.domain.usecase

import com.android.exomind.test.data.model.CityWeather
import com.android.exomind.test.data.repository.WeatherRepositoryImpl

/**
 * fetch weather by city UseCase
 */
class FetchCityWeatherUsesCase {
    suspend operator fun invoke(city: String): CityWeather? {
        return WeatherRepositoryImpl.fetchWeatherByCity(city)
    }
}