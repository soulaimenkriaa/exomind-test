package com.android.exomind.test.data.model

import com.google.gson.annotations.SerializedName

/**
 * Weather Api response for each city
 */
class CityWeather {
    @SerializedName("weather")
    var weathers: List<Weather>? = null

    @SerializedName("main")
    var main: Main? = null

    @SerializedName("name")
    var cityName: String? = null
}

class Weather {
    @SerializedName("main")
    var weatherMain: String? = null
}

class Main {
    @SerializedName("temp")
    var mainTemp: Double? = 0.0
}