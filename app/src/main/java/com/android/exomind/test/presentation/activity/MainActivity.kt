package com.android.exomind.test.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.exomind.test.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(root)
        }
    }
}