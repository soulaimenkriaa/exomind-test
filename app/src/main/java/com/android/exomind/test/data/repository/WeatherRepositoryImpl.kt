package com.android.exomind.test.data.repository

import com.android.exomind.test.data.model.CityWeather
import com.android.exomind.test.data.source.WeatherRemoteDataSource
import com.android.exomind.test.domain.repository.WeatherRepository

object WeatherRepositoryImpl : WeatherRepository {
    override suspend fun fetchWeatherByCity(city: String): CityWeather? {
        return WeatherRemoteDataSource.fetchWeatherByCity(city)
    }
}