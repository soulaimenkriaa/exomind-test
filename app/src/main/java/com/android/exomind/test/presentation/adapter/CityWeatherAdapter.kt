package com.android.exomind.test.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.exomind.test.R
import com.android.exomind.test.core.constant.GlobalConstants.CloudStatus.CLEAR
import com.android.exomind.test.data.model.CityWeather
import com.android.exomind.test.databinding.ItemCityWeatherBinding
import java.util.*

class CityWeatherAdapter :
    RecyclerView.Adapter<CityWeatherAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: ItemCityWeatherBinding) :
        RecyclerView.ViewHolder(binding.root)

    private var mCityWeathers = arrayListOf<CityWeather>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCityWeatherBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(mCityWeathers[position]) {
                binding.itemCityWeatherCityNameTextView.text = this.cityName
                binding.itemCityWeatherWeatherTemperatureTextView.text =
                    this.main?.mainTemp?.toInt().toString() + "°"

                binding.itemCityWeatherWeatherImageView.setImageResource(
                    if (this.weathers?.get(0)?.weatherMain?.equals(CLEAR)!!) {
                        R.drawable.ic_round_wb_sunny_24
                    } else {
                        R.drawable.ic_round_wb_cloudy_24
                    }
                )
            }
        }
    }

    fun setCityWeathers(cityWeathers: ArrayList<CityWeather>) {
        mCityWeathers.addAll(cityWeathers)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = mCityWeathers.size
}